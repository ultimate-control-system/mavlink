//
// Created by Magnus Kjelland on 02/10/2022.
//

#include "Backend.h"
#include <iostream>
Backend::Backend(QObject *parent) :
    QObject(parent),
    _sysId(0)

{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Backend::timerCallback);

    _timerHeartbeat = new QTimer(this);
    connect(_timerHeartbeat, &QTimer::timeout, this, &Backend::timerHeartbeatCallback);

    // Init serial
    QString result;
    //char *uart_name = (char*)"/dev/cu.usbserial-A801Z0ET";
    char *uart_name = (char*)"/dev/ttyUSB2";

    int baudrate = 57600;
    port = new Serial_Port(uart_name, baudrate);
    usleep(100);

    port->start();
    usleep(100);
    timer->start(1);
    _timerHeartbeat->start(500);
    if ( !port->is_running() ) // PORT_OPEN
    {
        qDebug()<<"Starting in simulation mode";
        _simMode = true;
        return;
    }

    _heartbeatCounter = 0;

}


void Backend::init()
{
    qCritical()<<"Crit";
    emit operate("");


    bool success;
    //while(not current_messages.sysid){


    //}
}

void Backend::reset()
{

}

void Backend::update()
{

}

Backend::~Backend() {
    timer->stop();
}
void Backend::timerHeartbeatCallback() {
    mavlink_message_t msg;
    mavlink_heartbeat_t data;
    data.custom_mode = _heartbeatCounter + 1;
    mavlink_msg_heartbeat_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
    qDebug()<<"sending heartbeat " << _heartbeatCounter;
}

void Backend::timerCallback() {

    mavlink_message_t message;
    if(!_simMode)
        success = port->read_message(message);


//qDebug()<<"recevied";
    // ----------------------------------------------------------------------
    //   HANDLE MESSAGE
    // ----------------------------------------------------------------------
    if(success && !_simMode) {
        current_messages.sysid  = message.sysid;
        current_messages.compid = message.compid;
        // Handle Message ID
        switch (message.msgid) {
            case MAVLINK_MSG_ID_HEARTBEAT: {
                //qDebug()<<"MAVLINK_MSG_ID_HEARTBEAT";
                mavlink_msg_heartbeat_decode(&message, &(current_messages.heartbeat));
                current_messages.time_stamps.heartbeat = get_time_usec();
                this_timestamps.heartbeat = current_messages.time_stamps.heartbeat;

                //qDebug()<<current_messages.heartbeat.base_mode;
                //qDebug()<<current_messages.heartbeat.system_status;
                _heartbeatCounter = current_messages.heartbeat.custom_mode;
                update_heartbeat("heartbeat", current_messages.sysid, current_messages.compid, current_messages.heartbeat.base_mode,current_messages.heartbeat.system_status);
                break;
            }
            case MAVLINK_MSG_ID_SENSOR_OFFSETS: {
                mavlink_msg_sensor_offsets_decode(&message, &(current_messages.sensor_offset));

                current_messages.time_stamps.attitude = get_time_usec();
                this_timestamps.attitude = current_messages.time_stamps.attitude;
                //qDebug()<<current_messages.sensor_offset.accel_cal_x;
                //qDebug()<<current_messages.sensor_offset.accel_cal_y;
                //qDebug()<<current_messages.sensor_offset.accel_cal_z;
                break;
            }
            case MAVLINK_MSG_ID_COMMAND_ACK: {
                break;
            }
            case MAVLINK_MSG_ID_PARAM_VALUE: {
                mavlink_param_value_t data;
                mavlink_msg_param_value_decode(&message, &data);
                _parameters[data.param_index] = data.param_value;
                switch(Param(data.param_index)) {
                    case timeout:
                        break;
                    case max_speed:
                        break;
                    case max_acc:
                        break;
                    case left_endstop:
                    emit endstopChanged();
                        break;
                    case right_endstop:
                    emit endstopChanged();
                        break;
                    case end_of_param:
                        break;
                }

                qDebug()<<data.param_value << " " << data.param_index << " of " << data.param_count;
                break;
            }
        }

    } else {
        //Transmit from buffer at a max rate!
        static unsigned long counter;
        if(!output_messages_.empty() && counter++ %10 == 1){
            qDebug()<<"empting buffer";
            if(!_simMode)
                port->write_message(output_messages_.back());
            output_messages_.pop_back();
        }
    }

}

void Backend::move_left() {
    createCommandMessage(Command::move_left);
    qDebug()<<"MoveLeft";
}

void Backend::move_right() {
    qDebug()<<"MoveRight";
    createCommandMessage(Command::move_right);
}

void Backend::arm() {
    qDebug()<<"arm command";
    createCommandMessage(Command::arm);
}

void Backend::disarm() {
    qDebug()<<"disarm command";
    createCommandMessage(Command::disarm);
}

void Backend::stop()
{
    qDebug()<<"Trying to stop";
    createCommandMessage(Command::stop);
}

void Backend::createCommandMessage(Command cmd){
    output_messages_.emplace_back();
    mavlink_command_long_t data;
    data.command = cmd;
    mavlink_msg_command_long_encode(sysId(),compId(),&output_messages_.back(), &data);
}

void Backend::set_acc(double new_value){
    mavlink_message_t msg;
    mavlink_param_set_t data;
    data.param_id[0] = Param::max_acc;
    data.param_value = new_value;
    mavlink_msg_param_set_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
}
void Backend::set_speed(double new_value){
    mavlink_message_t msg;
    mavlink_param_set_t data;
    data.param_id[0] = Param::max_speed;
    data.param_value = new_value;
    mavlink_msg_param_set_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
}
void Backend::get_acc(){
    mavlink_message_t msg;
    mavlink_param_request_read_t data;
    data.param_index = Param::max_acc;
    mavlink_msg_param_request_read_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);

}
void Backend::get_speed(){
    mavlink_message_t msg;
    mavlink_param_request_read_t data;
    data.param_index = Param::max_speed;
    mavlink_msg_param_request_read_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
}
void Backend::get_endstop() {
    mavlink_message_t msg;
    mavlink_param_request_read_t data;
    data.param_index = Param::left_endstop;
    mavlink_msg_param_request_read_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
    data.param_index = Param::right_endstop;
    mavlink_msg_param_request_read_encode(sysId(),compId(),&msg,&data);
    if(!_simMode)
        port->write_message(msg);
}
void Backend::setLeftEndstop(){
    createCommandMessage(Command::set_left_endstop);
}
void Backend::setRightEndstop(){
    createCommandMessage(Command::set_right_endstop);
}
