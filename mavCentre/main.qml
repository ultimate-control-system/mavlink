import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15

import mavlink 1.0


Window {
    id: root
    visible: true

    width: 1000
    height: width*9/16

    Text {
        id: hello
        text: mavlink.hello
        anchors.centerIn: parent
    }
    Column {
        padding: 10
        spacing: 10
        anchors{
            horizontalCenter: parent.horizontalCenter
        }
        Row {
            padding: 10
            spacing: 10
            anchors{
                horizontalCenter: parent.horizontalCenter
            }
            TextFieldLabel{
                labelText: qsTr("SysId:")
                textField: mavlink.sysId
                enableTextField: false
                boxWidth: 100
            }
            TextFieldLabel{
                labelText: qsTr("CompId:")
                textField: mavlink.compId
                enableTextField: false
                boxWidth: 100
            }
            TextFieldLabel{
                labelText: qsTr("Heartbeat:")
                textField: mavlink.heartbeatCounter
                enableTextField: false
                boxWidth: 150
            }
            TextFieldLabel{
                labelText: qsTr("Battery:")
                textField: mavlink.battery
                enableTextField: false
                boxWidth: 100
            }


        }
        Row {
            anchors{
                horizontalCenter: parent.horizontalCenter
            }
            padding: 10
            spacing: 10
            TextFieldLabel{
                labelText: qsTr("Left endstop:")
                textField: mavlink.leftEndstop
                enableTextField: false
                boxWidth: 100
            }
            TextFieldLabel{
                labelText: qsTr("Position:")
                textField: mavlink.position
                enableTextField: false
                boxWidth: 100
            }
            TextFieldLabel{
                labelText: qsTr("Speed:")
                textField: mavlink.speed
                enableTextField: false
                boxWidth: 100
            }
            TextFieldLabel{
                labelText: qsTr("Right endstop:")
                textField: mavlink.rightEndstop
                enableTextField: false
                boxWidth: 100
            }
            Button {
                text:"Set Left"
                onClicked: mavlink.setLeftEndstop()
            }
            Button {
                text:"Set Right"
                onClicked: mavlink.setRightEndstop()
            }
            Button {
                text:"Get Endstop"
                onClicked: mavlink.get_endstop()
            }
        }

        Row {

            anchors{
                horizontalCenter: parent.horizontalCenter
            }
            padding: 10
            spacing: 10
            Button {
                text:"Arm"
                onClicked: mavlink.arm()

            }
            Button {
                text:"Disarm"
                onClicked: mavlink.disarm()
            }
            Button {
                text:"Left"
                onClicked: {}
                onPressed: mavlink.move_left()
                onReleased: mavlink.stop()
            }
            Button {
                text:"Right"
                onClicked: {}
                onPressed: mavlink.move_right()
                onReleased: mavlink.stop()
            }
        }
        Row {
            padding: 10
            spacing: 10
            anchors{
                horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Speed:"
            }
            Slider {
                id: speed
                width:600
                from:0
                to:100
                value:25
            }
            Button {
                text:"Set"
                onClicked: mavlink.set_speed(speed.value)
            }
            Button {
                text:"Get"
                onClicked: mavlink.get_speed()
            }
        }
        Row {
            padding: 10
            spacing: 10
            anchors{
                horizontalCenter: parent.horizontalCenter
            }
            Label {
                text: "Acceleration:"
            }
            Slider {
                id: acceleration
                width:600
                from:0
                to:100
                value:25
            }
            Button {
                text:"Set"
                onClicked: mavlink.set_acc(acceleration.value)
            }
            Button {
                text:"Get"
                onClicked: mavlink.get_acc()
            }
        }
    }
    MavLink {
        id: mavlink

    }
    Component.onCompleted: {
        //mavlink.init();

    }
}
