//
// Created by Magnus Kjelland on 02/10/2022.
//

#ifndef HELLO_BACKEND_H
#define HELLO_BACKEND_H


#include <QObject>
#include <QTimer>
#include <QDebug>
#include <QThread>
#include <ardupilotmega/mavlink.h>
#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <cmath>
#include <fstream>
#include <sys/time.h>
#include "serial_port.h"
#include "../../transmission/pwm_test/include/data_types.h"

#define _USE_MATH_DEFINES
#include <cmath>
struct Time_Stamps
{
    Time_Stamps()
    {
        reset_timestamps();
    }

    uint64_t heartbeat;
    uint64_t sys_status;
    uint64_t battery_status;
    uint64_t radio_status;
    uint64_t local_position_ned;
    uint64_t global_position_int;
    uint64_t position_target_local_ned;
    uint64_t position_target_global_int;
    uint64_t highres_imu;
    uint64_t attitude;

    void
    reset_timestamps()
    {
        heartbeat = 0;
        sys_status = 0;
        battery_status = 0;
        radio_status = 0;
        local_position_ned = 0;
        global_position_int = 0;
        position_target_local_ned = 0;
        position_target_global_int = 0;
        highres_imu = 0;
        attitude = 0;
    }

};
struct Mavlink_Messages {
Q_GADGET
public:
    int sysid;
    int compid;

    // Heartbeat
    mavlink_heartbeat_t heartbeat;

    // System Status
    mavlink_sys_status_t sys_status;

    // Battery Status
    mavlink_battery_status_t battery_status;

    // Radio Status
    mavlink_radio_status_t radio_status;

    // Local Position
    mavlink_local_position_ned_t local_position_ned;

    // Global Position
    mavlink_global_position_int_t global_position_int;

    // Local Position Target
    mavlink_position_target_local_ned_t position_target_local_ned;

    // Global Position Target
    mavlink_position_target_global_int_t position_target_global_int;

    // HiRes IMU
    mavlink_highres_imu_t highres_imu;

    // Attitude
    mavlink_attitude_t attitude;

    mavlink_sensor_offsets_t sensor_offset;

    // System Parameters?

    // Time Stamps
    Time_Stamps time_stamps;

    void reset_timestamps()
    {
        time_stamps.reset_timestamps();
    }

};

class Backend : public QObject
{
    uint64_t get_time_usec()
    {
        static struct timeval _time_stamp;
        gettimeofday(&_time_stamp, NULL);
        return _time_stamp.tv_sec*1000000 + _time_stamp.tv_usec;
    }
Q_OBJECT
    Q_PROPERTY(double compId READ compId NOTIFY sysIdChanged)
    Q_PROPERTY(unsigned long heartbeatCounter READ heartbeatCounter NOTIFY sysIdChanged)
    Q_PROPERTY(double sysId READ sysId NOTIFY sysIdChanged)
    Q_PROPERTY(double leftEndstop READ leftEndstop NOTIFY endstopChanged)
    Q_PROPERTY(double rightEndstop READ rightEndstop NOTIFY endstopChanged)
    Q_PROPERTY(double position READ position NOTIFY stateChanged)
    Q_PROPERTY(double speed READ speed NOTIFY stateChanged)
    Q_PROPERTY(double battery READ battery NOTIFY stateChanged)

    Q_PROPERTY(QString hello MEMBER _hello NOTIFY helloChanged)
    Q_PROPERTY(Mavlink_Messages messageStruct READ messageStruct NOTIFY messageStructChanged)


public:
    explicit Backend(QObject *parent = nullptr);

    virtual ~Backend();

    void timerCallback();
    void timerHeartbeatCallback();

    Q_INVOKABLE
    void init();

    Q_INVOKABLE
    void set_speed(double new_value);
    Q_INVOKABLE
    void get_endstop();
    Q_INVOKABLE
    void get_speed();
    Q_INVOKABLE
    void set_acc(double new_value);
    Q_INVOKABLE
    void get_acc();
    Q_INVOKABLE
    void stop();
    Q_INVOKABLE
    void reset();
    Q_INVOKABLE
    void move_left();
    Q_INVOKABLE
    void move_right();
    Q_INVOKABLE
    void arm();
    Q_INVOKABLE
    void disarm();
    Q_INVOKABLE
    void setLeftEndstop();
    Q_INVOKABLE
    void setRightEndstop();

    int sysId()const { return _sysId;}
    int compId()const { return _compId;}
    double speed()const { return 0.0;}
    double position()const { return 0.0;}
    double battery()const { return 0.0;}
    long heartbeatCounter() const { return _heartbeatCounter;}

    double leftEndstop()const { return _parameters[Param::left_endstop];}
    double rightEndstop()const { return _parameters[Param::right_endstop];}
    Mavlink_Messages messageStruct() const {
        return _msg;}


signals:
    void helloChanged(QString val);
    void messageStructChanged(Mavlink_Messages);
    void compIdChanged(double val);
    void sysIdChanged();
    void stateChanged();
    void endstopChanged();
    void operate(const QString &);
    //void stoperate(double a);

private slots:
    void update();
private:
    unsigned long _heartbeatCounter;
    double _parameters[Param::end_of_param];
    void createCommandMessage(Command cmd);
    std::vector<mavlink_message_t> output_messages_;
    void update_heartbeat(const QString & res, int sysId, int compId, int base_mode, int system_status){
    _sysId = sysId;
    _hello = res;
    _compId = compId;
    _msg.sensor_offset.mag_declination =1.23;
    Mavlink_Messages _msg;
    //emit helloChanged(res);
    emit sysIdChanged();
    //emit messageStructChanged(_msg);
    };
private:
    Generic_Port *port;
    Time_Stamps this_timestamps;
    Mavlink_Messages current_messages;
    QString _hello;
    double _sysId;
    int _compId;
    Mavlink_Messages _msg;
    QTimer* _timer;
    QTimer* _timerHeartbeat;
    QTimer* timer;
    bool success;
    bool _simMode{};
    int last_msg = -1;
};


#endif //HELLO_BACKEND_H
